#pragma once

#include <Windows.h>
#include <Xinput.h>

// Inicializamos funciones para gamepads
typedef DWORD WINAPI TInputGetState(DWORD dwUserIndex, XINPUT_STATE *pState);
typedef DWORD WINAPI TInputSetState(DWORD dwUserIndex, XINPUT_VIBRATION *pVibration);

DWORD WINAPI InputGetStateStub(DWORD dwUserIndex, XINPUT_STATE *pState)
{
	return ERROR_DEVICE_NOT_CONNECTED;
}

DWORD WINAPI InputSetStateStub(DWORD dwUserIndex, XINPUT_VIBRATION *pVibration)
{
	return ERROR_DEVICE_NOT_CONNECTED;
}

static TInputGetState* InputGetState = InputGetStateStub;
static TInputSetState* InputSetState = InputSetStateStub;