#pragma once

#include "..\RenderManager.h"
#include "..\Input\ActionManager.h"

namespace engine
{
	class CCameraController
	{
	public:
		CCameraController() :m_Position(0, 0, 0), m_Front(0, 0, 1), m_Up(0, 1, 0) {}
		virtual ~CCameraController() {}
		virtual void Update(float ElapsedTime) = 0;
		virtual void RenderDebugGUI() { ; };
		virtual void HandleActions(CActionManager *actionManager) { ; };
		virtual void Render(CRenderManager *renderManager) { ; };

		void SetToRenderManager(CRenderManager &_RenderManager) const
		{
			_RenderManager.SetViewMatrix(m_Position, m_Position + m_Front, m_Up);
		}

	protected:
		Vect3f m_Position;
		Vect3f m_Front;
		Vect3f m_Up;
	};
}