#include "RenderManager.h"
#ifdef _DEBUG
#include "Utils\MemLeaks\MemLeaks.h"
#endif

namespace engine {
	CRenderManager::CRenderManager()
	{

	}

	CRenderManager::~CRenderManager()
	{
		Destroy();
		m_D3D11Debug.get()->ReportLiveDeviceObjects(D3D11_RLDO_DETAIL);
		m_D3D11Debug.reset(nullptr);
	}

	void CRenderManager::Destroy()
	{
		m_Device.reset(nullptr);
		m_DeviceContext.reset(nullptr);
		m_SwapChain.reset(nullptr);
		m_RenderTargetView.reset(nullptr);
		m_DepthStencil.reset(nullptr);
		m_DepthStencilView.reset(nullptr);
		m_SolidRenderState.reset(nullptr);

		m_DebugVertexBuffer.reset(nullptr);
		m_DebugVertexShader.reset(nullptr);
		m_DebugVertexLayout.reset(nullptr);
		m_DebugPixelShader.reset(nullptr);
	}

	bool CRenderManager::Init(HWND hWnd, int width, int height)
	{
#ifdef _DEBUG
		m_BackgroundColor = Vect4f(1.0f, 0.0f, 1.0f, 1.0f);
#else
		m_BackgroundColor = Vect4f(0.0f, 0.0f, 0.0f, 0.0f);
#endif

		DXGI_SWAP_CHAIN_DESC sd;
		ZeroMemory(&sd, sizeof(sd));
		sd.BufferCount = 1;
		sd.BufferDesc.Width = width;
		sd.BufferDesc.Height = height;
		sd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
		sd.BufferDesc.RefreshRate.Numerator = 60;
		sd.BufferDesc.RefreshRate.Denominator = 1;
		sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
		sd.OutputWindow = hWnd;
		sd.SampleDesc.Count = 1;
		sd.SampleDesc.Quality = 0;
		sd.Windowed = true;

		// Definimos versiones de DirectX aceptadas
		D3D_FEATURE_LEVEL featureLevels[] =
		{
			D3D_FEATURE_LEVEL_11_0,
			D3D_FEATURE_LEVEL_10_1,
			D3D_FEATURE_LEVEL_10_0
		};

		UINT numFeatureLevels = ARRAYSIZE(featureLevels);

		IDXGISwapChain *l_SwapChain;
		ID3D11Device *l_Device;
		ID3D11DeviceContext *l_DeviceContext;
		// Creamos los device y el swap chain
		bool d3ddebug;
#ifdef _DEBUG
		d3ddebug = true;
#else
		d3ddebug = false;
#endif
		if (FAILED(D3D11CreateDeviceAndSwapChain(NULL, D3D_DRIVER_TYPE_HARDWARE, NULL, d3ddebug ? D3D11_CREATE_DEVICE_DEBUG : 0, featureLevels, numFeatureLevels,
			D3D11_SDK_VERSION, &sd, &l_SwapChain, &l_Device, NULL, &l_DeviceContext)))
		{
			return false;
		}
		m_SwapChain.reset(l_SwapChain);
		m_Device.reset(l_Device);
		m_DeviceContext.reset(l_DeviceContext);
		ID3D11Debug *l_D3D11Debug;
		HRESULT hr;
		hr = l_Device->QueryInterface(__uuidof(ID3D11Debug), reinterpret_cast<void**>(&l_D3D11Debug));
		if (FAILED(hr))
		{
			return false;
		}
		m_D3D11Debug.reset(l_D3D11Debug);

		IDXGIFactory *dgxiFactory;
		hr = m_SwapChain.get()->GetParent(__uuidof(IDXGIFactory), (void**)&dgxiFactory);
		if (FAILED(hr))
		{
			return false;
		}

		hr = dgxiFactory->MakeWindowAssociation(hWnd, DXGI_MWA_NO_ALT_ENTER);
		if (FAILED(hr))
		{
			return false;
		}
		dgxiFactory->Release();

		bool success;

		success = InitStencil(width, height);
		if (success == false) return false;		

		success = CreateDebugShader();
			
		if (success == false) return false;

		success = CreateDebugVertexBuffer();

		if (success == false) return false;

		CreateDebugObjects();

		return true;
	}

	bool CRenderManager::InitStencil(int width, int height)
	{
		HRESULT hr;
		// Definimos el lienzo
		ID3D11Texture2D *pBackBuffer;
		auto sc = m_SwapChain.get();
		if (FAILED(sc->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&pBackBuffer)))
		{
			return false;
		}
		ID3D11RenderTargetView *l_RenderTargetView;
		hr = m_Device->CreateRenderTargetView(pBackBuffer, NULL, &l_RenderTargetView);
		m_RenderTargetView.reset(l_RenderTargetView);
		pBackBuffer->Release();
		if (FAILED(hr))
		{
			return false;
		}

		// Creamos la textura de profundidad
		D3D11_TEXTURE2D_DESC descDepth;
		ZeroMemory(&descDepth, sizeof(descDepth));
		descDepth.Width = width;
		descDepth.Height = height;
		descDepth.MipLevels = 1;
		descDepth.ArraySize = 1;
		descDepth.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
		descDepth.SampleDesc.Count = 1;
		descDepth.SampleDesc.Quality = 0;
		descDepth.Usage = D3D11_USAGE_DEFAULT;
		descDepth.BindFlags = D3D11_BIND_DEPTH_STENCIL;
		descDepth.CPUAccessFlags = 0;
		descDepth.MiscFlags = 0;
		ID3D11Texture2D *l_DepthStencil;
		hr = m_Device->CreateTexture2D(&descDepth, NULL, &l_DepthStencil);
		if (FAILED(hr))
		{
			return false;
		}


		// Creamos la vista asociada
		D3D11_DEPTH_STENCIL_VIEW_DESC descDSV;
		ZeroMemory(&descDSV, sizeof(descDSV));
		descDSV.Format = descDepth.Format;
		descDSV.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
		descDSV.Texture2D.MipSlice = 0;
		ID3D11DepthStencilView *l_DepthStencilView;
		hr = m_Device->CreateDepthStencilView(l_DepthStencil, &descDSV, &l_DepthStencilView);
		if (FAILED(hr))
		{
			return false;
		}
		m_DepthStencil.reset(l_DepthStencil);
		m_DepthStencilView.reset(l_DepthStencilView);

		// Definimos el viewport de la aplicación
		D3D11_VIEWPORT vp;
		vp.Width = (FLOAT)width;
		vp.Height = (FLOAT)height;
		vp.MinDepth = 0.0f;
		vp.MaxDepth = 1.0f;
		vp.TopLeftX = 0;
		vp.TopLeftY = 0;
		m_DeviceContext->RSSetViewports(1, &vp);

		// Activamos el render target
		auto dsv = m_DepthStencilView.get();
		auto rtv = m_RenderTargetView.get();
		m_DeviceContext->OMSetRenderTargets(1, &l_RenderTargetView, l_DepthStencilView);
		//m_RenderTargetView.reset(rtv);
	}

	ID3D11Device* CRenderManager::GetDevice() const
	{
		return m_Device.get();
	}

	ID3D11DeviceContext* CRenderManager::GetDeviceContext() const
	{
		return m_DeviceContext.get();
	}

	void CRenderManager::BeginRender()
	{
		auto rtv = m_RenderTargetView.get();
		auto dsv = m_DepthStencilView.get();
		m_DeviceContext->ClearRenderTargetView(rtv , &m_BackgroundColor.x);
		m_DeviceContext->ClearDepthStencilView(dsv, D3D11_CLEAR_DEPTH, 1.0f, 0);
		this->SetSolidRenderState();
	}

	void CRenderManager::SetSolidRenderState()
	{
		auto srs = m_SolidRenderState.get();
		m_DeviceContext->RSSetState(srs);
	}

	void CRenderManager::EndRender()
	{
		m_SwapChain->Present(1, 0);
	}

	bool CRenderManager::CreateDebugShader()
	{
		// C++ macros are nuts
		#define STRINGIFY(X) #X
		#define TOSTRING(X) STRINGIFY(X)

		const char debugRenderEffectCode[] =
		"#line " TOSTRING(__LINE__) "\n"
		"struct VS_OUTPUT\n"
		"{\n"
		"	float4 Pos : SV_POSITION;\n"
		"	float4 Color : COLOR0;\n"
		"};\n"
		"\n"
		"VS_OUTPUT VS(float4 Pos : POSITION, float4 Color : COLOR)\n"
		"{\n"
		"	VS_OUTPUT l_Output = (VS_OUTPUT)0;\n"
		"	l_Output.Pos = Pos;\n"
		"	l_Output.Color = Color;\n"
		"	return l_Output;\n"
		"}\n"
		"\n"
		"float4 PS(VS_OUTPUT IN) : SV_Target\n"
		"{\n"
		"	//return float4(1,0,0,1);\n"
		"	//return m_BaseColor;\n"
		"	return IN.Color;\n"
		"}\n";

		HRESULT hr;
		ID3DBlob *vsBlob, *psBlob;
		ID3DBlob *errorBlob;
		hr = D3DCompile(debugRenderEffectCode, sizeof(debugRenderEffectCode), __FILE__, nullptr, nullptr,
			"VS", "vs_4_0", 0, 0, &vsBlob, &errorBlob);

		if (FAILED(hr))
		{
			if (&errorBlob != NULL)
			{
				OutputDebugStringA((char*)errorBlob->GetBufferPointer());
			}
			if (errorBlob) errorBlob->Release();
			return false;
		}

		hr = D3DCompile(debugRenderEffectCode, sizeof(debugRenderEffectCode), __FILE__, nullptr, nullptr,
			"PS", "ps_4_0", 0, 0, &psBlob, &errorBlob);

		if (FAILED(hr))
		{
			if (&errorBlob != NULL)
			{
				OutputDebugStringA((char*)errorBlob->GetBufferPointer());
			}
			if (errorBlob) errorBlob->Release();
			return false;
		}

		ID3D11VertexShader *l_DebugVertexShader;
		hr = m_Device->CreateVertexShader(vsBlob->GetBufferPointer(), vsBlob->GetBufferSize(), NULL, &l_DebugVertexShader);
		if (FAILED(hr))
		{
			return false;
		}
		m_DebugVertexShader.reset(l_DebugVertexShader);

		ID3D11PixelShader *l_DebugPixelShader;
		hr = m_Device->CreatePixelShader(psBlob->GetBufferPointer(), psBlob->GetBufferSize(), NULL, &l_DebugPixelShader);
		if (FAILED(hr))
		{
			return false;
		}
		m_DebugPixelShader.reset(l_DebugPixelShader);

		D3D11_INPUT_ELEMENT_DESC layout[] = {
			{"POSITION", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0},
			{"COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 16, D3D11_INPUT_PER_VERTEX_DATA, 0 }
		};

		ID3D11InputLayout *l_DebugVertexLayout;
		hr = m_Device->CreateInputLayout(layout, 2, vsBlob->GetBufferPointer(), vsBlob->GetBufferSize(), &l_DebugVertexLayout);
		m_DebugVertexLayout.reset(l_DebugVertexLayout);

		if (FAILED(hr))
		{
			return false;
		}

		return true;
	}

	bool CRenderManager::CreateDebugVertexBuffer()
	{
		D3D11_BUFFER_DESC l_BufferDescription;
		ZeroMemory(&l_BufferDescription, sizeof(l_BufferDescription));
		l_BufferDescription.Usage = D3D11_USAGE_DYNAMIC;
		l_BufferDescription.ByteWidth = sizeof(CDebugVertex)*DebugVertexBufferSize;
		l_BufferDescription.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		l_BufferDescription.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;

		HRESULT hr;
		ID3D11Buffer *l_DebugVertexBuffer;
		hr = m_Device->CreateBuffer(&l_BufferDescription, nullptr, &l_DebugVertexBuffer);
		m_DebugVertexBuffer.reset(l_DebugVertexBuffer);

		if (FAILED(hr))
		{
			return false;
		}

		return true;
	}

	void CRenderManager::CreateDebugObjects()
	{
		// Triangle
		static CDebugVertex resultBuffer[3] = {
			{ Vect4f(-0.5f, -0.5f, 0.0f, 1.0f), CColor(1.0f, 0.0f, 0.0f, 1.0f) },
			{ Vect4f(0.0f, 0.5f, 0.0f, 1.0f), CColor(0.0f, 0.0f, 1.0f, 1.0f) },
			{ Vect4f(0.5f, -0.5f, 0.0f, 1.0f), CColor(0.0f, 1.0f, 0.0f, 1.0f) }
		};
		m_TriangleRenderableVertexs = resultBuffer;
		m_NumVerticesTriangle = 3;

		// Axis
		static CDebugVertex l_AxisVtxs[6] = {
			{ Vect4f(0.0f, 0.0f, 0.0f, 1.0f), CColor(1.0f, 0.0f, 0.0f, 1.0f) },
			{ Vect4f(1.0f, 0.0f, 0.0f, 1.0f), CColor(1.0f, 0.0f, 0.0f, 1.0f) },

			{ Vect4f(0.0f, 0.0f, 0.0f, 1.0f), CColor(0.0f, 1.0f, 0.0f, 1.0f) },
			{ Vect4f(0.0f, 1.0f, 0.0f, 1.0f), CColor(0.0f, 1.0f, 0.0f, 1.0f) },

			{ Vect4f(0.0f, 0.0f, 0.0f, 1.0f), CColor(0.0f, 0.0f, 1.0f, 1.0f) },
			{ Vect4f(0.0f, 0.0f, 1.0f, 1.0f), CColor(0.0f, 0.0f, 1.0f, 1.0f) }
		};

		m_AxisRenderableVertexs = l_AxisVtxs;
		m_NumVerticesAxis = 6;

		// Grid
		const int grid_size = 10;
		static CDebugVertex l_GridVtxs[(grid_size + 1) * 2 * 2];
		CColor gridColor = CColor(1.0f, 1.0f, 1.0f, 1.0f);
		for (int i = 0; i <= grid_size; ++i)
		{
			float x = (((float)i / (float)grid_size) - 0.5f) * 2;
			l_GridVtxs[i*2].Position = Vect4f(x, 0.0f, -1.0f, 1.0f);
			l_GridVtxs[i*2].Color = gridColor;
			l_GridVtxs[i*2+1].Position = Vect4f(x, 0.0f, 1.0f, 1.0f);
			l_GridVtxs[i*2+1].Color = gridColor;
		}
		for (int i = 0; i <= grid_size; ++i)
		{
			float z = (((float)i / (float)grid_size) - 0.5f) * 2;
			l_GridVtxs[(grid_size + 1) * 2 + i * 2].Position = Vect4f(-1.0f, 0.0f, z, 1.0f);
			l_GridVtxs[(grid_size + 1) * 2 + i * 2].Color = gridColor;
			l_GridVtxs[(grid_size + 1) * 2 + i * 2 + 1].Position = Vect4f(1.0f, 0.0f, z, 1.0f);
			l_GridVtxs[(grid_size + 1) * 2 + i * 2 + 1].Color = gridColor;
		}

		m_GridRenderableVertexs = l_GridVtxs;
		m_NumVerticesGrid = (grid_size + 1) * 2 * 2;

		// Cube
		const float cube_size = 1.0f;
		const float cube_half = cube_size / 2.0f;
		CColor cube_color = CColor(1.0f, 1.0f, 1.0f, 1.0f);
		static CDebugVertex l_CubeVtxs[24] = {
			{ Vect4f(-cube_half, -cube_half, cube_half, 1.0f), cube_color },
			{ Vect4f(-cube_half, cube_half, cube_half, 1.0f), cube_color },

			{ Vect4f(-cube_half, -cube_half, cube_half, 1.0f), cube_color },
			{ Vect4f(cube_half, -cube_half, cube_half, 1.0f), cube_color },

			{ Vect4f(cube_half, -cube_half, cube_half, 1.0f), cube_color },
			{ Vect4f(cube_half, cube_half, cube_half, 1.0f), cube_color },

			{ Vect4f(-cube_half, cube_half, cube_half, 1.0f), cube_color },
			{ Vect4f(cube_half, cube_half, cube_half, 1.0f), cube_color },

			{ Vect4f(-cube_half, -cube_half, -cube_half, 1.0f), cube_color },
			{ Vect4f(-cube_half, cube_half, -cube_half, 1.0f), cube_color },

			{ Vect4f(-cube_half, -cube_half, -cube_half, 1.0f), cube_color },
			{ Vect4f(cube_half, -cube_half, -cube_half, 1.0f), cube_color },

			{ Vect4f(cube_half, -cube_half, -cube_half, 1.0f), cube_color },
			{ Vect4f(cube_half, cube_half, -cube_half, 1.0f), cube_color },

			{ Vect4f(-cube_half, cube_half, -cube_half, 1.0f), cube_color },
			{ Vect4f(cube_half, cube_half, -cube_half, 1.0f), cube_color },

			{ Vect4f(-cube_half, cube_half, cube_half, 1.0f), cube_color },
			{ Vect4f(-cube_half, cube_half, -cube_half, 1.0f), cube_color },

			{ Vect4f(cube_half, cube_half, cube_half, 1.0f), cube_color },
			{ Vect4f(cube_half, cube_half, -cube_half, 1.0f), cube_color },

			{ Vect4f(-cube_half, -cube_half, cube_half, 1.0f), cube_color },
			{ Vect4f(-cube_half, -cube_half, -cube_half, 1.0f), cube_color },

			{ Vect4f(cube_half, -cube_half, cube_half, 1.0f), cube_color },
			{ Vect4f(cube_half, -cube_half, -cube_half, 1.0f), cube_color },
		};
		m_CubeRenderableVertexs = l_CubeVtxs;
		m_NumVerticesCube = 24;

		// Sphere

		const int l_Aristas = 20; //10
		static CDebugVertex l_SphereVtxs[4 * l_Aristas*l_Aristas];
		for (int t = 0; t<l_Aristas; ++t)
		{
			float l_RadiusRing = sin(/*DEG2RAD*/(6.28318531f / 360.f) * (180.0f*((float)t)) / ((float)l_Aristas));
			for (int b = 0; b<l_Aristas; ++b)
			{
				l_SphereVtxs[(t*l_Aristas * 4) + (b * 4) + 0].Position = Vect4f(l_RadiusRing*cos(/*DEG2RAD*/(6.28318531f / 360.f) *((float)(360.0f*(float)b) / ((float)l_Aristas))), cos(/*DEG2RAD*/(6.28318531f / 360.f) *(180.0f*((float)t)) / ((float)l_Aristas)), l_RadiusRing*sin(/*DEG2RAD*/(6.28318531f / 360.f) *((float)(360.0f*(float)b) / ((float)l_Aristas))), 1.0f);
				l_SphereVtxs[(t*l_Aristas * 4) + (b * 4) + 0].Color = CColor(1.0f, 1.0f, 1.0f, 1.0f);
				l_SphereVtxs[(t*l_Aristas * 4) + (b * 4) + 1].Position = Vect4f(l_RadiusRing*cos(/*DEG2RAD*/(6.28318531f / 360.f) *((float)(360.0f*(float)(b + 1)) / ((float)l_Aristas))), cos(/*DEG2RAD*/(6.28318531f / 360.f) *(180.0f*((float)t)) / ((float)l_Aristas)), l_RadiusRing*sin(/*DEG2RAD*/(6.28318531f / 360.f) *((float)(360.0f*(float)(b + 1)) / ((float)l_Aristas))), 1.0f);
				l_SphereVtxs[(t*l_Aristas * 4) + (b * 4) + 1].Color = CColor(1.0f, 1.0f, 1.0f, 1.0f);

				float l_RadiusNextRing = sin(/*DEG2RAD*/(6.28318531f / 360.f) *(180.0f*((float)(t + 1))) / ((float)l_Aristas));

				l_SphereVtxs[(t*l_Aristas * 4) + (b * 4) + 2].Position = Vect4f(l_RadiusRing*cos(/*DEG2RAD*/(6.28318531f / 360.f) *((float)(360.0f*(float)b) / ((float)l_Aristas))), cos(/*DEG2RAD*/(6.28318531f / 360.f) *(180.0f*((float)t)) / ((float)l_Aristas)), l_RadiusRing*sin(/*DEG2RAD*/(6.28318531f / 360.f) *((float)(360.0f*(float)b) / ((float)l_Aristas))), 1.0f);
				l_SphereVtxs[(t*l_Aristas * 4) + (b * 4) + 2].Color = CColor(1.0f, 1.0f, 1.0f, 1.0f);
				l_SphereVtxs[(t*l_Aristas * 4) + (b * 4) + 3].Position = Vect4f(l_RadiusNextRing*cos(/*DEG2RAD*/(6.28318531f / 360.f) *((float)(360.0f*(float)b) / ((float)l_Aristas))), cos(/*DEG2RAD*/(6.28318531f / 360.f) *(180.0f*((float)(t + 1))) / ((float)l_Aristas)), l_RadiusNextRing*sin(/*DEG2RAD*/(6.28318531f / 360.f) *((float)(360.0f*(float)b) / ((float)l_Aristas))), 1.0f);
				l_SphereVtxs[(t*l_Aristas * 4) + (b * 4) + 3].Color = CColor(1.0f, 1.0f, 1.0f, 1.0f);
			}
		}
		m_SphereRenderableVertexs = l_SphereVtxs;
		m_NumVerticesSphere = 4 * l_Aristas*l_Aristas;
	}

	void CRenderManager::DrawTriangle()
	{
		DrawDebug(m_TriangleRenderableVertexs, m_NumVerticesTriangle, D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	}

	void CRenderManager::DrawAxis(const float SizeX, const float SizeY, const float SizeZ, const float rotation, const CColor color)
	{
		Mat44f scale;
		scale.SetIdentity();
		scale.Scale(SizeX, SizeY, SizeZ);
		scale.RotByAngleY(rotation);
		scale = scale * m_ModelViewProjectionMatrix;
		DebugRender(scale, m_AxisRenderableVertexs, m_NumVerticesAxis, color);
	}

	void CRenderManager::DrawGrid(const float SizeX, const float SizeY, const float SizeZ, const float rotation, const CColor color)
	{
		Mat44f scale;
		scale.SetIdentity();
		scale.Scale(SizeX, SizeY, SizeZ);
		scale.RotByAngleY(rotation);
		scale = /*rotation * */scale * m_ModelViewProjectionMatrix;
		DebugRender(scale, m_GridRenderableVertexs, m_NumVerticesGrid, color);
	}

	void CRenderManager::DrawCube(const float SizeX, const float SizeY, const float SizeZ, const float rotation, const CColor color)
	{
		Mat44f scale;
		scale.SetIdentity();
		scale.Scale(SizeX, SizeY, SizeZ);
		scale.RotByAngleY(rotation);
		scale = scale * m_ModelViewProjectionMatrix;
		DebugRender(scale, m_CubeRenderableVertexs, m_NumVerticesCube, color);
	}

	void CRenderManager::DrawSphere(float Radius, const float rotation, const CColor color, const Vect3f position)
	{
		Mat44f scale;
		scale.SetIdentity();
		scale.Scale(Radius, Radius, Radius);
		scale.RotByAngleY(rotation);
		scale.SetPos(position);
		scale = scale *  m_ModelViewProjectionMatrix;
		DebugRender(scale, m_SphereRenderableVertexs, m_NumVerticesSphere, color);
	}

	void CRenderManager::DrawDebug(const CDebugVertex* points, int vertices, D3D11_PRIMITIVE_TOPOLOGY topology)
	{
		// Set vertex data
		D3D11_MAPPED_SUBRESOURCE resource;
		auto dvb = m_DebugVertexBuffer.get();
		HRESULT hr = m_DeviceContext->Map(dvb, 0, D3D11_MAP_WRITE_DISCARD, 0, &resource);

		if (FAILED(hr)) return;

		memcpy(resource.pData, points, vertices * sizeof(CDebugVertex));
		m_DeviceContext->Unmap(dvb, 0);

		UINT stride = sizeof(CDebugVertex);
		UINT offset = 0;
		ID3D11Buffer *l_DebugVertexBuffer;
		m_DeviceContext->IASetVertexBuffers(0, 1, &l_DebugVertexBuffer, &stride, &offset);
		m_DebugVertexBuffer.reset(l_DebugVertexBuffer);
		m_DeviceContext->IASetPrimitiveTopology(topology);
		auto dvl = m_DebugVertexLayout.get();
		auto dvs = m_DebugVertexShader.get();
		auto dps = m_DebugPixelShader.get();
		m_DeviceContext->IASetInputLayout(dvl);
		m_DeviceContext->VSSetShader(dvs, NULL, 0);
		m_DeviceContext->PSSetShader(dps, NULL, 0);

		m_DeviceContext->Draw(vertices, 0);
	}

	void CRenderManager::DebugRender(const Mat44f& modelViewProj, const CDebugVertex* modelVertices, int numVertices, CColor colorTint)
	{
		CDebugVertex *resultBuffer = (CDebugVertex *)alloca(numVertices * sizeof(CDebugVertex));

		for (int i = 0; i < numVertices; ++i)
		{
			resultBuffer[i].Position = (modelVertices[i].Position * modelViewProj);

			resultBuffer[i].Color = modelVertices[i].Color * colorTint;
		}

		// set vertex data
		D3D11_MAPPED_SUBRESOURCE resource;
		auto dvb = m_DebugVertexBuffer.get();
		HRESULT hr = m_DeviceContext->Map(dvb, 0, D3D11_MAP_WRITE_DISCARD, 0, &resource);

		if (FAILED(hr))
			return; // TODO log

		memcpy(resource.pData, resultBuffer, numVertices * sizeof(CDebugVertex));

		m_DeviceContext->Unmap(dvb, 0);


		UINT stride = sizeof(CDebugVertex);
		UINT offset = 0;
		ID3D11Buffer *l_DebugVertexBuffer = m_DebugVertexBuffer.get();
		m_DeviceContext->IASetVertexBuffers(0, 1, &l_DebugVertexBuffer, &stride, &offset);
		m_DebugVertexBuffer.reset(l_DebugVertexBuffer);
		m_DeviceContext->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_LINELIST);
		auto dvl = m_DebugVertexLayout.get();
		auto dvs = m_DebugVertexShader.get();
		auto dps = m_DebugPixelShader.get();
		m_DeviceContext->IASetInputLayout(dvl);
		m_DeviceContext->VSSetShader(dvs, NULL, 0);
		m_DeviceContext->PSSetShader(dps, NULL, 0);

		m_DeviceContext->Draw(numVertices, 0);
	}

	void CRenderManager::SetModelMatrix(const Mat44f &Model)
	{
		m_ModelMatrix = Model;
		m_ModelViewProjectionMatrix = m_ModelMatrix * m_ViewProjectionMatrix;
	}

	void CRenderManager::SetViewMatrix(const Mat44f &View)
	{
		m_ViewMatrix = View;
		m_ViewProjectionMatrix = m_ViewMatrix * m_ProjectionMatrix;
		m_ModelViewProjectionMatrix = m_ModelMatrix * m_ViewProjectionMatrix;
	}

	void CRenderManager::SetViewMatrix(const Vect3f &vPos, const Vect3f &vTarget, const Vect3f &vUp)
	{
		Mat44f View;
		View.SetIdentity();
		View.SetFromLookAt(vPos, vTarget, vUp);
		SetViewMatrix(View);
	}

	void CRenderManager::SetProjectionMatrix(const Mat44f &Projection)
	{
		m_ProjectionMatrix = Projection;
		m_ViewProjectionMatrix = m_ViewMatrix * m_ProjectionMatrix;
		m_ModelViewProjectionMatrix = m_ModelMatrix * m_ViewProjectionMatrix;
	}

	void CRenderManager::SetProjectionMatrix(float fovy, float aspect, float zn, float zf)
	{
		Mat44f Projection;
		Projection.SetIdentity();
		Projection.SetFromPerspective(fovy, aspect, zn, zf);
		SetProjectionMatrix(Projection);
	}

	void CRenderManager::SetProjectionMatrix(float fovy, int aspectWidth, int aspectHeight, float zn, float zf)
	{
		float aspect = (float)((float)aspectWidth / (float)aspectHeight);
		SetProjectionMatrix(fovy, aspect, zn, zf);
	}

	void CRenderManager::SetViewProjectionMatrix(const Mat44f &View, const Mat44f &Projection)
	{
		m_ViewMatrix = View;
		m_ProjectionMatrix = Projection;
		m_ViewProjectionMatrix = m_ViewMatrix * m_ProjectionMatrix;
		m_ModelViewProjectionMatrix = m_ModelMatrix * m_ViewProjectionMatrix;
	}

	void CRenderManager::Resize(int Width, int Height)
	{
		if (m_Device.get() != nullptr)
		{
			m_DeviceContext.get()->OMSetRenderTargets(0, nullptr, nullptr);

			
			m_RenderTargetView.reset(nullptr);
			m_DepthStencil.reset(nullptr);
			m_DepthStencilView.reset(nullptr);
			
			auto l_SwapChain = m_SwapChain.get();
			l_SwapChain->ResizeBuffers(0, Width, Height, DXGI_FORMAT_R8G8B8A8_UNORM, 0);
			//l_SwapChain->ResizeBuffers(0, Width, Height, DXGI_FORMAT_UNKNOWN, 0);
			InitStencil(Width, Height);
			SetProjectionMatrix(0.8f, (float)Width / (float)Height, 0.5f, 100.0f);
		}
	}
}