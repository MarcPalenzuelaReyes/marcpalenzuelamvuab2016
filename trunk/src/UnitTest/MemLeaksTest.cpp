#ifdef _DEBUG
#include <gtest/gtest.h>

#include "Base/Utils/MemLeaks/MemLeaks.h"


namespace unit_test {
	class Element {

	};
	TEST(MemLeaksTest, MemLeakTest1) {
		MemLeaks::MemoryBegin();

		Element* element = new Element();

		MemLeaks::MemoryEnd();
	}

	TEST(MemLeaksTest, MemLeakTest2) {
		MemLeaks::MemoryBegin();

		{
			std::auto_ptr<Element> element(new Element());
		}

		MemLeaks::MemoryEnd();
	}
}
#endif