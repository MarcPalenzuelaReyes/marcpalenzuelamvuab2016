#include <gtest/gtest.h>
#include <memory>

namespace unit_test {
	class Valor
	{
	public:
		Valor() : valor(0){};
		~Valor() {
			valor = 0;
		}
	private:
		unsigned int valor;
	};

	TEST(RAIITest, TestRAII1) {
		std::auto_ptr<Valor> valor(new Valor());
		valor.reset(new Valor());
	}

	TEST(RAIITest, TestRAII2) {
		std::unique_ptr<Valor> valor(new Valor());
		valor.reset(new Valor());
	}
}